#-------------------------------------------------------------------------------------------------
# File: Trex_Dino_Run_Game.py
#
# Author: Dakshvir Singh Rehill
# Date: 09/02/2021
#
# Summary: This is the entry point of the game that will be responsible for
# initial launch and final cleanup of the game
#-------------------------------------------------------------------------------------------------
from Core.Systems.GameEngine import GameEngine
import Game.GameConfig as game_config
from Game.DinoRunManager import DinoRunManager

def main():
    game_manager = DinoRunManager()
    game_engine = GameEngine(game_manager,
                             game_config.WINDOW_SIZE,
                             game_config.RECT_FOCUS_SURFACE,
                             game_config.WINDOW_DEFAULT_COLOR,
                             game_config.WINDOW_NAME,
                             game_config.AUDIO_PATH,
                             game_config.MIXER_VOLUME)
    game_engine.game_loop()

if __name__ == '__main__':
    main()