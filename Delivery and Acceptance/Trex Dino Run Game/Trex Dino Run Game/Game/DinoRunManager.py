#-------------------------------------------------------------------------------------------------
# File: DinoRunManager.py
#
# Author: Dakshvir Singh Rehill
# Date: 11/02/2021
#
# Summary: Manager responsible for all workings of the dino game including the bot functionality
#-------------------------------------------------------------------------------------------------
from Core.Systems.GameManager import GameManager
from Core.Systems.RenderSystem import RenderSystem
from Core.Systems.CollisionSystem import CollisionSystem
from Core.Systems.AudioSystem import AudioSystem
import Game.GameConfig as game_config
import pygame,pygame_gui
from Game.Modes.RegularMode import RegularMode
from Game.Modes.TrainMode import TrainMode

class DinoRunManager(GameManager):
    """Manager responsible for all workings of the dino game including the bot functionality"""
    def __init__(self):
        pass

    def initialize(self):
        """Initialize Requirements of the Game"""
        self.set_ui_manager(game_config.WINDOW_SIZE,game_config.UI_THEME_PATH,game_config.UI_LIVE_UPDATE)
        self.create_menu_panels()
        self.regular_mode = None
        self.train_mode = None
        self.game_state = 0
        self.menu_sound = False
        self.game_over = False
        self.populate_hud_menu()
        self.populate_game_over_menu()
        self.populate_main_menu()

    def toggle_menu_sound(self,play):
        if self.menu_sound == play:
            return
        self.menu_sound = play
        if self.menu_sound:
            AudioSystem.get_instance().play_sound(game_config.MAIN_MENU_SOUND,-1)
        else:
            AudioSystem.get_instance().stop_sound(game_config.MAIN_MENU_SOUND)

    def update(self,time_delta,pending_frames,pending_frame_lock,predictions,prediction_lock,is_training,is_simulated,train_mode,quit,system_ready):
        """Update all game objects and move accordingly"""
        self.ui_manager.update(time_delta)
        self.toggle_menu_sound(self.game_state == 0)
        #use game states to check ui updates
        if self.game_state == 0:
            self.check_main_menu_buttons()
        elif self.game_state == 1:
            if self.regular_mode is None:
                self.score_label.set_text("0")
                self.show_hud_menu()
                self.regular_mode = RegularMode(self)
            else:
                mode_update = self.regular_mode.update(time_delta)
                if not mode_update:#game over
                    if self.game_over:
                        if self.back_button.check_pressed():
                            AudioSystem.get_instance().play_sound(game_config.BUTTON_CLICK_SOUND)
                            CollisionSystem.get_instance().clear_system()
                            RenderSystem.get_instance().clear_system()
                            self.regular_mode.cleanup()
                            self.regular_mode = None
                            self.game_over = False
                            self.game_state = 0
                            self.show_main_menu()
                    else:
                        self.game_over = True
                        self.show_game_over_menu()
        elif self.game_state == 2 or self.game_state == 3:
            if self.train_mode is None:
                self.score_label.set_text("0")
                self.show_hud_menu()
                self.train_mode = TrainMode(self,self.game_state == 3)
            else:
                self.game_over = not self.train_mode.update(time_delta,
                                                            pending_frames,
                                                            pending_frame_lock,
                                                            predictions,
                                                            prediction_lock,
                                                            is_training,
                                                            is_simulated,
                                                            train_mode,
                                                            quit,
                                                            system_ready)
                if self.game_over:
                    CollisionSystem.get_instance().clear_system()
                    RenderSystem.get_instance().clear_system()
                    self.train_mode.cleanup()
                    self.train_mode = None
                    self.game_over = False
                    self.game_state = 0
                    self.show_main_menu()
        
    def check_main_menu_buttons(self):
        """Function to check main menu button presses"""
        if self.regular_button.check_pressed():
            AudioSystem.get_instance().play_sound(game_config.BUTTON_CLICK_SOUND)
            #launch regular game
            self.game_state = 1
        if self.view_button.check_pressed():
            AudioSystem.get_instance().play_sound(game_config.BUTTON_CLICK_SOUND)
            #launch view mode
            self.game_state = 2
        if self.fresh_button.check_pressed():
            AudioSystem.get_instance().play_sound(game_config.BUTTON_CLICK_SOUND)
            #launch fresh train mode
            self.game_state = 3

    def create_menu_panels(self):
        """Function to create menu panels that will be used in the game"""
        self.main_menu_panel = pygame_gui.elements.UIPanel(pygame.Rect((0,0),game_config.WINDOW_SIZE)
                                                           ,10,self.ui_manager)
        self.hud_menu_panel = pygame_gui.elements.UIPanel(pygame.Rect((0,0),game_config.WINDOW_SIZE)
                                                          , 5, self.ui_manager)
        self.game_over_menu_panel = pygame_gui.elements.UIPanel(pygame.Rect((0,0),game_config.WINDOW_SIZE)
                                                                , 8, self.ui_manager)
        self.main_menu_panel.disable()
        self.game_over_menu_panel.disable()
        self.hud_menu_panel.disable()

    def populate_main_menu(self):
        """Function to create all interactive elements in the main menu panel"""
        self.main_menu_panel.enable()
        self.hud_menu_panel.hide()
        self.game_over_menu_panel.hide()
        pygame_gui.elements.UIImage(pygame.Rect((325,30),game_config.SIZE_GAME_LOGO)
                                    ,pygame.image.load(game_config.IMG_GAME_LOGO),
                                    self.ui_manager,self.main_menu_panel.get_container())

        button_container = pygame_gui.elements.UIPanel(pygame.Rect((415,170),
                                                (game_config.SIZE_MAIN_MENU_BUTTON[0],
                                                 game_config.SIZE_MAIN_MENU_BUTTON[1] * 3 
                                                 + game_config.MAIN_MENU_GAP * 4)),
                                    2,self.ui_manager,container=self.main_menu_panel.get_container())

        self.regular_button = pygame_gui.elements.UIButton(pygame.Rect((0,game_config.MAIN_MENU_GAP)
                                                                       ,game_config.SIZE_MAIN_MENU_BUTTON),
                                                           game_config.TXT_REG_BTN,self.ui_manager,
                                                           button_container.get_container())

        spacing = game_config.MAIN_MENU_GAP * 2 + game_config.SIZE_MAIN_MENU_BUTTON[1]
        self.view_button = pygame_gui.elements.UIButton(pygame.Rect((0,spacing),game_config.SIZE_MAIN_MENU_BUTTON),
                                                        game_config.TXT_VIEW_BTN,self.ui_manager,
                                                        button_container.get_container())

        spacing = spacing + game_config.MAIN_MENU_GAP + game_config.SIZE_MAIN_MENU_BUTTON[1]
        self.fresh_button = pygame_gui.elements.UIButton(pygame.Rect((0,spacing),game_config.SIZE_MAIN_MENU_BUTTON),
                                                         game_config.TXT_FRSH_BTN,self.ui_manager,
                                                         button_container.get_container())

        logo_location = (game_config.WINDOW_SIZE[0] - game_config.COMPANY_LOGO_SPACING[0] - game_config.SIZE_COMPANY_LOGO[0],
                         game_config.WINDOW_SIZE[1] - game_config.COMPANY_LOGO_SPACING[1] - game_config.SIZE_COMPANY_LOGO[1])

        pygame_gui.elements.UIImage(pygame.Rect(logo_location,game_config.SIZE_COMPANY_LOGO),
                                    pygame.image.load(game_config.IMG_COMPANY_LOGO),
                                    self.ui_manager, self.main_menu_panel.get_container())

    def show_hud_menu(self):
        self.main_menu_panel.disable()
        self.main_menu_panel.hide()
        self.hud_menu_panel.enable()
        self.hud_menu_panel.show()

    def populate_hud_menu(self):
        """Function to create all elements in hud menu panel"""
        self.score_label = pygame_gui.elements.UILabel(pygame.Rect((1030,30),game_config.SIZE_SCORE_LABEL),
                                                       "0",self.ui_manager,self.hud_menu_panel.get_container())

    def show_game_over_menu(self):
        self.game_over_menu_panel.enable()
        self.game_over_menu_panel.show()

    def show_main_menu(self):
        self.game_over_menu_panel.disable()
        self.game_over_menu_panel.hide()
        self.hud_menu_panel.disable()
        self.hud_menu_panel.hide()
        self.main_menu_panel.enable()
        self.main_menu_panel.show()

    def populate_game_over_menu(self):
        btn_pos = game_config.WINDOW_SIZE
        btn_pos = int(btn_pos[0] * 0.5),int(btn_pos[1] * 0.5)
        self.back_button = pygame_gui.elements.UIButton(pygame.Rect(btn_pos,game_config.SIZE_RESTART_BUTTON),
                                                        game_config.TXT_RESTART_BTN,self.ui_manager,
                                                        self.game_over_menu_panel.get_container())

