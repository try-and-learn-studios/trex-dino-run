#-------------------------------------------------------------------------------------------------
# File: PropObject.py
#
# Author: Dakshvir Singh Rehill
# Date: 27/02/2021
#
# Summary: This class has convenience function to display prop objects at game
# speeds
#-------------------------------------------------------------------------------------------------
from Core.Objects.GameObject import GameObject
from Core.Systems.RenderSystem import RenderSystem
import Game.GameConfig as game_config
import pygame

class PropObject(GameObject):
    """This class has convenience function to display background"""
    def __init__(self, img, position, move_speed, scale):
        super().__init__()
        self.reset_object(img,position,move_speed,scale)
        RenderSystem.get_instance().register_drawable(self)
        self.is_dirty = True

    def reset_object(self, img, position, move_speed, scale):
        self.rect = img.get_rect()
        self.size = (int(self.rect[2] * scale),int(self.rect[3] * scale)) 
        self.surface = pygame.transform.scale(img,self.size)
        self.position = position
        self.move_speed = move_speed
        self.rect[0] = self.position[0]
        self.rect[1] = self.position[1]

    def cleanup(self):
        RenderSystem.get_instance().remove_drawable(self.instance_id)

    def update(self, time_delta):
        self.position[0] = int(self.position[0] + self.move_speed * time_delta)
        self.rect[0] = self.position[0]