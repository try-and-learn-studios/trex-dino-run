#-------------------------------------------------------------------------------------------------
# File: BackgroundObject.py
#
# Author: Dakshvir Singh Rehill
# Date: 27/02/2021
#
# Summary: This class has convenience function to display background
#-------------------------------------------------------------------------------------------------
from Core.Objects.GameObject import GameObject
from Core.Systems.RenderSystem import RenderSystem
from Game.GameObjects.PropObject import PropObject
import random
import Game.GameConfig as game_config
import pygame

class BackgroundObject(GameObject):
    """This class has convenience function to display background"""
    def __init__(self):
        super().__init__()
        self.random_seeds = game_config.RANDOM_SEEDS
        self.surface = pygame.image.load(game_config.IMG_BACKGROUND)
        self.prop_imgs = []
        for path in game_config.IMG_PROPS:
            prop_obj = pygame.image.load(path).convert()
            prop_obj.set_colorkey(prop_obj.get_at((0,0)), pygame.RLEACCEL)
            self.prop_imgs.append(prop_obj)        
        self.rect = self.surface.get_rect()
        self.speed = 0
        RenderSystem.get_instance().register_drawable(self)
        self.is_dirty = True
        self.props = []
        self.deleted_props = []
        self.reset_to_start()


    def reset_to_start(self):
        random.shuffle(self.random_seeds)
        random.seed(random.choice(self.random_seeds))
        self.position = [0,0]
        self.rect[0] = self.position[0]
        self.rect[1] = self.position[1]
        self.last_time = game_config.PROP_SPAWN_TIME
        remove_list = self.props
        for prop in remove_list:
            self.clean_prop(prop)

    def clean_prop(self,prop):
        prop.is_dirty = False
        self.props.remove(prop)
        self.deleted_props.append(prop)

    def update(self, time_delta):
        self.last_time = self.last_time + time_delta
        if self.last_time >= game_config.PROP_SPAWN_TIME and len(self.props) < game_config.MAX_PROPS:
            ix = random.randrange(0,len(self.prop_imgs))
            position = [game_config.WINDOW_SIZE[0] + 150, random.randrange(0,250)]
            if len(self.deleted_props) > 0:
                prop_object = self.deleted_props.pop(0)
                prop_object.is_dirty = True
                prop_object.reset_object(self.prop_imgs[ix],position,-self.speed,game_config.SCALE_PROP)
            else:
                prop_object = PropObject(self.prop_imgs[ix],position,-self.speed,game_config.SCALE_PROP)
            self.props.append(prop_object)
            self.last_time = 0
        remove_list = []
        for prop in self.props:
            prop.update(time_delta)
            if (prop.position[0] <= -(prop.size[0] * 0.5)): #check if prop off screen
                remove_list.append(prop)
        for prop in remove_list:
            self.clean_prop(prop)

    def cleanup(self):
        RenderSystem.get_instance().remove_drawable(self.instance_id)
        for prop in self.props:
            prop.cleanup()
        for prop in self.deleted_props:
            prop.cleanup()
        self.prop = []
        self.deleted_props = []


                


