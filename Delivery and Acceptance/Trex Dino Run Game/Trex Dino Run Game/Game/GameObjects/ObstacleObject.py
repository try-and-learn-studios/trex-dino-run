#-------------------------------------------------------------------------------------------------
# File: ObstacleObject.py
#
# Author: Dakshvir Singh Rehill
# Date: 27/02/2021
#
# Summary: This class has convenience function to move obstacle objects at game
# speeds
#-------------------------------------------------------------------------------------------------
from Game.GameObjects.PropObject import PropObject
from Core.Objects.ColliderObject import ColliderObject
import Game.GameConfig as game_config
import pygame

class ObstacleObject(PropObject):
    """This class has convenience function to display background"""
    def __init__(self, img, position, move_speed, scale):
        self.collider = None
        self.jump_pos_col = None
        super().__init__(img,position,move_speed,scale)
        collider_size = (int(self.rect[2]*0.8),int(self.rect[3] * 0.8))
        self.offset = [int(self.rect[2] - collider_size[0]),int(self.rect[3] - collider_size[1])]
        pos = [self.position[0] + self.offset[0],self.position[1] + self.offset[1]]
        self.collider = ColliderObject(collider_size,pos,"Obstacle")
        pos[0] = pos[0] - 2 * collider_size[0] - 70
        collider_size = (int(collider_size[0] * 2 + 20),collider_size[1])
        self.jump_pos_col = ColliderObject(collider_size,pos,"Jump Point")
        #self.jump_pos_col.debug_mode = True
        #self.collider.debug_mode = True

    def reset_object(self,img,position, move_speed, scale):
        super().reset_object(img,position,move_speed,scale)
        if self.collider is None:
            return
        collider_size = (int(self.rect[2]*0.8),int(self.rect[3] * 0.8))
        self.offset = [int(self.rect[2] - collider_size[0]),int(self.rect[3] - collider_size[1])]
        pos = [self.position[0] + self.offset[0],self.position[1] + self.offset[1]]
        self.collider.reset_object(collider_size,pos)
        pos[0] = pos[0] - 2*collider_size[0] - 70
        collider_size = (int(collider_size[0] * 2 + 20),collider_size[1])
        self.jump_pos_col.reset_object(collider_size,pos)


    def update(self, time_delta):
        super().update(time_delta)
        self.collider.rect[0] = self.rect[0] + self.offset[0]
        self.collider.rect[1] = self.rect[1] + self.offset[1]
        self.jump_pos_col.rect[0] = self.collider.rect[0] - 2*self.collider.rect[2] - 70
        self.jump_pos_col.rect[1] = self.collider.rect[1]

    def cleanup(self):
        super().cleanup()
        self.collider.cleanup()
        self.collider = None