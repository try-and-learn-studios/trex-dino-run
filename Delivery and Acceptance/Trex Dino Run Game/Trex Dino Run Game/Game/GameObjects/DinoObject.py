#-------------------------------------------------------------------------------------------------
# File: DinoObject.py
#
# Author: Dakshvir Singh Rehill
# Date: 20/02/2021
#
# Summary: This class has convenience functions to allow dino to move in the
# game
#-------------------------------------------------------------------------------------------------
from Core.Objects.GameObject import GameObject
from Core.Objects.SpriteSheet import SpriteSheet
from Core.Objects.ColliderObject import ColliderObject
from Core.Systems.RenderSystem import RenderSystem
from Core.Systems.AudioSystem import AudioSystem
import Game.GameConfig as game_config
import pygame

class DinoObject(GameObject):
    """This class has convenience functions to allow dino to move in the game"""
    def __init__(self):
        super().__init__()
        self.spritesheet = SpriteSheet(game_config.SPRITESHEET_DINO)
        self.dead_sprites = self.spritesheet.load_strip(pygame.Rect(0,0,680,472),8,-1)
        self.idle_sprites = self.spritesheet.load_strip(pygame.Rect(0,472,680,472),10,-1)
        self.jump_sprites = self.spritesheet.load_strip(pygame.Rect(0,944,680,472),12,-1)
        self.run_sprites = self.spritesheet.load_strip(pygame.Rect(0,1416,680,472),8,-1)
        self.go_down_sprites = self.dead_sprites
        self.stay_down_sprites = self.go_down_sprites[-3:]
        self.go_up_sprites = self.dead_sprites[::-1]
        self.position = [30,360]        
        self.size = (680 * game_config.SCALE_FACTOR,472 * game_config.SCALE_FACTOR)
        self.size = (int(self.size[0]),int(self.size[1]))
        col_size = (int(self.size[0] * 0.3),int(self.size[1] * 0.75))
        self.offset = [30,(self.size[1] - col_size[1]) - 20]
        pos = [self.position[0] + self.offset[0],self.position[1] + self.offset[1]]
        self.collider = ColliderObject(col_size,pos,"Dino")
        #self.collider.debug_mode = True
        self.reset_to_start()
        RenderSystem.get_instance().register_drawable(self)
        self.jump_height = 30 * game_config.GRAVITY_Y * (game_config.JUMP_TIME * (len(self.jump_sprites) - 1))

    def reset_to_start(self):
        self.active_anim = self.idle_sprites
        self.cur_ix = 0
        self.position = [30,360]
        img = self.active_anim[self.cur_ix]
        self.cur_ix = self.cur_ix + 1
        self.surface = pygame.transform.scale(img,self.size)
        self.rect = self.surface.get_rect()
        self.rect[0] = self.position[0]
        self.rect[1] = self.position[1]
        self.collider.rect[0] = self.rect[0] + self.offset[0]
        self.collider.rect[1] = self.rect[1] + self.offset[1]
        self.last_time = 0
        self.is_dirty = True
        self.is_jumping = False
        self.down_mode = False
        self.is_down = False
        self.is_dead = False

    def update(self, time_delta):
        if not self.is_dead:
            self.check_collision()
        self.last_time += time_delta
        if (self.last_time >= game_config.ANIM_TIME and not self.is_jumping) or (self.last_time >= game_config.JUMP_TIME and self.is_jumping):
            img = self.active_anim[self.cur_ix]
            if self.is_jumping:
                self.update_jump_rect()
            self.cur_ix = (self.cur_ix + 1) % len(self.active_anim)
            if self.is_dead and self.cur_ix == 0:
                self.cur_ix = len(self.active_anim) - 1
            if self.cur_ix == 0 and self.is_jumping: #if jumping and reached last of sprite jump is over
                self.is_jumping = False
                self.start_run()
            if self.cur_ix == 0 and self.down_mode and not self.is_down: #if going down and still need to be down
                self.is_down = True
                self.active_anim = self.stay_down_sprites
            if self.active_anim == self.go_up_sprites and self.cur_ix == 0:
                self.start_run()
            self.surface = pygame.transform.scale(img,self.size)
            self.last_time = 0
        self.rect = self.surface.get_rect()
        self.rect[0] = self.position[0]
        self.rect[1] = self.position[1]
        self.collider.rect[0] = self.rect[0] + self.offset[0]
        self.collider.rect[1] = self.rect[1] + self.offset[1]


    def check_collision(self):
        if self.is_dead:
            return
        if self.collider.in_collision:
            for tag in self.collider.collided_with:
                if "Obstacle" in tag:
                    self.dino_dead()
                    break



    def update_jump_rect(self):
        max_len = int(len(self.active_anim) * 0.5)
        ix = abs(max_len - self.cur_ix)
        per = 1 - float(ix) / float(max_len)
        if per < 0.2 and self.cur_ix > max_len:
            per = 0
        self.position[1] = 360 - int(per * self.jump_height)

    def start_run(self):
        self.cur_ix = 0
        self.active_anim = self.run_sprites
        self.last_time = game_config.ANIM_TIME

    def start_jump(self):
        if self.is_jumping:
            return -0.5
        self.cur_ix = 0
        self.active_anim = self.jump_sprites
        self.last_time = game_config.JUMP_TIME
        self.is_jumping = True
        reward = 10 if self.is_within_jump() else -0.1
        if reward > 0:
            AudioSystem.get_instance().play_sound(game_config.SUCCESS_JUMP_SOUND)
        else:
            AudioSystem.get_instance().play_sound(game_config.JUMP_SOUND)
        return reward

    def is_within_jump(self):
        if self.collider.in_collision:
            for tag in self.collider.collided_with:
                if "Jump Point" in tag:
                    return True
        return False

    def dino_dead(self):
        AudioSystem.get_instance().play_sound(game_config.HIT_SOUND)
        self.is_dead = True
        self.cur_ix = 0
        self.active_anim = self.dead_sprites
        self.last_time = game_config.ANIM_TIME

    def dino_down(self):
        if not self.down_mode: #first call
            self.down_mode = True
            self.cur_ix = 0
            self.active_anim = self.go_down_sprites
            self.last_time = game_config.ANIM_TIME
            self.swap_collision()

    def swap_collision(self,mode=1):
        self.collider.swap_rect()
        self.offset[1] = self.offset[1] + 50 * mode


    def dino_up(self):
        self.down_mode = False
        self.is_down = False
        self.cur_ix = 0
        self.active_anim = self.go_up_sprites
        self.last_time = game_config.ANIM_TIME
        self.swap_collision(-1)

    def cleanup(self):
        self.collider.cleanup()
        RenderSystem.get_instance().remove_drawable(self.instance_id)


