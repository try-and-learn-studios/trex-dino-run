#-------------------------------------------------------------------------------------------------
# File: RegularMode.py
#
# Author: Dakshvir Singh Rehill
# Date: 17/02/2021
#
# Summary: This class is where the regular mode of the game runs
#-------------------------------------------------------------------------------------------------
from Core.Systems.InputSystem import InputSystem
from Game.Modes.Mode import Mode
import pygame

class RegularMode(Mode):
    """This class is where the regular mode of the game runs"""
    def __init__(self, game_manager):
        super().__init__(game_manager)

    def update(self,time_delta):
        if not self.player_character.is_dead:
            if self.down_mode:
                if not InputSystem.get_instance().is_key_pressed(pygame.K_DOWN):
                    self.down_mode = False
                    self.player_character.dino_up()
            else:
                if InputSystem.get_instance().is_key_pressed(pygame.K_SPACE) or InputSystem.get_instance().is_key_pressed(pygame.K_UP):
                    if not self.game_started:
                        self.game_started = True
                    self.player_character.start_jump()
                #commented out down mode due to training complexities
                #elif InputSystem.get_instance().is_key_pressed(pygame.K_DOWN):
                #    if self.game_started:
                #        self.down_mode = True
                #        self.player_character.dino_down()
            return super().update(time_delta)
        #game over
        self.player_character.update(time_delta)
        return False
    
    def cleanup(self):
        super().cleanup()
