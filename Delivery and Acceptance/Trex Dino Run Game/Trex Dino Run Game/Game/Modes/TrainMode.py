#-------------------------------------------------------------------------------------------------
# File: TrainMode.py
#
# Author: Dakshvir Singh Rehill
# Date: 06/03/2021
#
# Summary: This class is where the train mode of the game runs
#-------------------------------------------------------------------------------------------------
from Game.Modes.Mode import Mode
from Core.Systems.RenderSystem import RenderSystem
from Core.Systems.AudioSystem import AudioSystem
from Core.Systems.RenderSystem import show_img
from Core.Systems.InputSystem import InputSystem
import Game.GameConfig as game_config
import pygame
import random
from multiprocess import Process,Queue,Lock,Value
from ctypes import c_bool

class TrainMode(Mode):
    """This class is where the train mode of the game runs"""
    def __init__(self, game_manager, train_mode = True):
        super().__init__(game_manager)
        self.skip_frame = False
        self.train_mode = train_mode
        self.debug = True
        if train_mode and self.debug:
            self.create_train_frame_process()
            RenderSystem.get_instance().toggle_renderer(False)
            AudioSystem.get_instance().toggle_mixer(False)


    def create_train_frame_process(self):
        self.train_frame_lock = Lock()
        self.train_frame_queue = Queue()
        self.train_frame_close = Value(c_bool,False)
        self.train_frame_close.value = False
        train_frame_process = Process(target=show_img,args=(self.train_frame_close,self.train_frame_queue,self.train_frame_lock))
        train_frame_process.daemon = True
        train_frame_process.start()

    def restart_game(self):
        self.game_started = True
        self.player_character.start_jump()

    def start_simulation(self,time_delta):
        self.restart_game()
        self.get_state(0,time_delta)


    def update(self, time_delta,\
       pending_frames,pending_frame_lock,predictions,prediction_lock,is_training,is_simulated,train_mode,quit,system_ready):
        if self.skip_frame:
            self.skip_frame = False
            self.reset_to_start()
            self.restart_game()
            return True

        if not system_ready.value or is_training.value:
            return True

        if not self.game_started:
            self.start_simulation(time_delta)
            train_mode.value = self.train_mode
            is_simulated.value = True
            return True

        action_index = 0
        if not predictions.empty():
            if prediction_lock.acquire(False):
                while not predictions.empty():
                    action_index = predictions.get_nowait()
                prediction_lock.release()
        reward = self.get_state(action_index, time_delta)
        x_t = RenderSystem.get_instance().get_display_array()
        if not pending_frame_lock.acquire(False):
            return True
        pending_frames.put_nowait((reward,x_t,self.current_score))
        pending_frame_lock.release()
        if self.train_mode and self.debug:
            if self.train_frame_lock.acquire(False):
                self.train_frame_queue.put_nowait(x_t)
                self.train_frame_lock.release()
        if InputSystem.get_instance().is_key_pressed(pygame.K_SPACE):
            is_simulated.value = False
            if self.train_mode and self.debug:
                self.train_frame_close.value = True
                RenderSystem.get_instance().toggle_renderer()
                AudioSystem.get_instance().toggle_mixer()
            return False
        return True

    def get_state(self,action,time_delta):
        if action == 1:# and not self.player_character.down_mode:
            reward = self.player_character.start_jump()
        else:
            reward = -0.3 if self.player_character.is_within_jump() else 0.2
        #commented out down mode due to training complexities
        #elif action == 2 and not self.player_character.down_mode:
        #    self.player_character.dino_down()
        #elif action == 3 and self.player_character.down_mode:
        #    self.player_character.dino_up()
        super().update(time_delta)
        if self.player_character.is_dead:
            self.skip_frame = True
            reward = -10
        return reward

    def cleanup(self):
        # add any cleanup code here
        if self.train_mode and self.debug:
            self.train_frame_close.value = True
        super().cleanup()

