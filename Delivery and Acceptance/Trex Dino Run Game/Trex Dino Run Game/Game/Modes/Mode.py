#-------------------------------------------------------------------------------------------------
# File: Mode.py
#
# Author: Dakshvir Singh Rehill
# Date: 06/03/2021
#
# Summary: Base class of all modes to setup game's common functionalities
#-------------------------------------------------------------------------------------------------
from Core.Systems.InputSystem import InputSystem
from Core.Systems.AudioSystem import AudioSystem
from Game.GameObjects.DinoObject import DinoObject
from Game.GameObjects.BackgroundObject import BackgroundObject
from Game.GameObjects.ObstacleObject import ObstacleObject
import Game.GameConfig as game_config
import pygame
import random

class Mode():
    """Base class of all modes to setup game's common functionalities"""
    def __init__(self,game_manager): #function to initialize brand new regular mode
        self.random_seeds = game_config.RANDOM_SEEDS
        self.game_manager = game_manager
        self.background_object = BackgroundObject()
        self.player_character = DinoObject()
        self.obstacles = []
        self.deleted_obstacles = []
        self.fly_obstacle = pygame.image.load(game_config.IMG_FLYOBSTACLE).convert()
        self.fly_obstacle.set_colorkey(self.fly_obstacle.get_at((0,0)), pygame.RLEACCEL)
        self.ground_obstacles = []
        for path in game_config.IMG_GROUNDOBSTACLES:
            ground_obstacle = pygame.image.load(path).convert()
            ground_obstacle.set_colorkey(ground_obstacle.get_at((0,0)), pygame.RLEACCEL)
            self.ground_obstacles.append(ground_obstacle)
        self.sound_playing = False
        self.reset_to_start()

    def toggle_sound(self):
        if self.sound_playing == self.game_started:
            return
        self.sound_playing = self.game_started
        if self.sound_playing:
            AudioSystem.get_instance().play_sound(game_config.RUN_SOUND,-1)
        else:
            AudioSystem.get_instance().stop_sound(game_config.RUN_SOUND)

    def reset_to_start(self):
        random.shuffle(self.random_seeds)
        random.seed(random.choice(self.random_seeds))
        self.game_started = False
        self.toggle_sound()
        self.down_mode = False
        self.current_level = 1
        self.current_score = 0
        self.current_time = 0
        self.level_time = game_config.LEVEL_TIME
        self.speed = game_config.BASE_LEVEL_SPEED
        self.background_object.speed = self.speed
        self.obstacle_spawn_time = 0
        obstacle_cpy = self.obstacles
        for obstacle in obstacle_cpy:
            self.clean_obstacle(obstacle)
        self.game_manager.score_label.set_text("0")
        self.background_object.reset_to_start()
        self.player_character.reset_to_start()


    def update(self,time_delta):
        self.toggle_sound()
        if not self.game_started:
                return True
        self.current_score = int(self.current_score + self.current_level * game_config.BASE_SCORE_INCREMENT * time_delta)
        self.game_manager.score_label.set_text(str(self.current_score))
        self.current_time = self.current_time + time_delta
        if self.current_time >= self.level_time : #level up
            self.level_up()
        self.background_object.update(time_delta)
        self.player_character.update(time_delta)
        self.manage_obstacles(time_delta)
        return True

    def clean_obstacle(self,obstacle):
        obstacle.collider.enabled = False
        obstacle.is_dirty = False
        obstacle.collider.debug_mode = False
        self.deleted_obstacles.append(obstacle)
        self.obstacles.remove(obstacle)

    def level_up(self):
        self.current_level = self.current_level + 1
        self.speed = self.speed + game_config.LEVEL_SPEED_INCREMENT * self.current_level * game_config.LEVEL_INCREMENT_FACTOR
        for obstacle in self.obstacles:
            obstacle.speed = self.speed
        self.background_object.speed = self.speed

    def manage_obstacles(self,time_delta):
        remove_list = []
        for obstacle in self.obstacles:
            if (obstacle.position[0] <= -(obstacle.size[0] * 0.5)): #check if obstacles have gone off screen
                remove_list.append(obstacle)
        for obstacle in remove_list:
            self.clean_obstacle(obstacle)

        self.obstacle_spawn_time = self.obstacle_spawn_time + time_delta

        for obstacle in self.obstacles:
            obstacle.update(time_delta)

        if self.obstacle_spawn_time >= game_config.OBSTACLE_SPAWN_TIME:
            position = [game_config.WINDOW_SIZE[0] + 150, random.randrange(250,400)]
            img = self.fly_obstacle
            scale = game_config.SCALE_FLY
            if random.random() >= 0: #always show ground obstacles due to training complexities
                position[1] = 400
                ix = random.randrange(0,len(self.ground_obstacles))
                img = self.ground_obstacles[ix]
                scale = game_config.SCALE_GROUNDOBSTACLES[ix]
            if len(self.deleted_obstacles) > 0:
                obstacle_object = self.deleted_obstacles.pop(0)
                obstacle_object.reset_object(img,position,-self.speed,scale)
                obstacle_object.collider.enabled = True
                obstacle_object.is_dirty = True
                #obstacle_object.collider.debug_mode = True
            else:
                obstacle_object = ObstacleObject(img,position,-self.speed,scale)
            self.obstacles.append(obstacle_object)
            self.obstacle_spawn_time = 0

    def cleanup(self):
        for obstacle in self.obstacles:
            obstacle.cleanup()
        for obstacle in self.deleted_obstacles:
            obstacle.cleanup()
        self.deleted_obstacles = []
        self.obstacles = []
        self.background_object.cleanup()
        self.background_object = None
        self.player_character.cleanup()
        self.player_character = None
        self.game_started = False
        self.toggle_sound()


