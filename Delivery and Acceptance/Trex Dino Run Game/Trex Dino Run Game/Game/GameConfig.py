#-------------------------------------------------------------------------------------------------
# File: GameConfig.py
#
# Author: Dakshvir Singh Rehill
# Date: 10/02/2021
#
# Summary: File created to have a global configuration for the game to ensure
# data driven practices
#-------------------------------------------------------------------------------------------------
WINDOW_NAME = "Trex Dino AI Run"
WINDOW_SIZE = 1280,720
WINDOW_DEFAULT_COLOR = 0,0,0

RANDOM_SEEDS = [100,50,25,2,10,85,34,12,8,5,48,193,582,4,1,7]

UI_THEME_PATH = "Game/Assets/default_ui_theme.json"
UI_LIVE_UPDATE = True

SIZE_RESTART_BUTTON = 300,100
TXT_RESTART_BTN = "Restart"


IMG_COMPANY_LOGO = "Game/Assets/company_logo.png"
SIZE_COMPANY_LOGO = 200,200
IMG_GAME_LOGO = "Game/Assets/game_logo.png"
SIZE_GAME_LOGO = 630,100
SIZE_MAIN_MENU_BUTTON = 450,110
MAIN_MENU_GAP = 45
COMPANY_LOGO_SPACING = 50,50

TXT_REG_BTN = "Regular Mode"
TXT_VIEW_BTN = "View Mode"
TXT_GHOST_BTN = "Ghost Mode"
TXT_FRSH_BTN = "Fresh Mode"

SIZE_SCORE_LABEL = 200,100

SPRITESHEET_DINO = "Game/Assets/DinoSheet.png"
IMG_BACKGROUND = "Game/Assets/Background.png"
IMG_FLYOBSTACLE = "Game/Assets/FlyObstacle.png"
IMG_GROUNDOBSTACLES = ["Game/Assets/GroundObstacleLarge1.png",
    "Game/Assets/GroundObstacleLarge2.png",
    "Game/Assets/GroundObstacleSmall.png"]
IMG_PROPS = ["Game/Assets/Bone1.png",
    "Game/Assets/Bone3.png",
    "Game/Assets/Bone4.png",
    "Game/Assets/Bone2.png"]

SCALE_PROP = 1
SCALE_FLY = 1
SCALE_GROUNDOBSTACLES = [1,
    1,
    1]

ANIM_TIME = 0.1
JUMP_TIME = 0.07
SCALE_FACTOR = 0.3
GRAVITY_Y = 10

LEVEL_TIME = 50
BASE_SCORE_INCREMENT = 50

BASE_LEVEL_SPEED = 300
LEVEL_SPEED_INCREMENT = 30

LEVEL_INCREMENT_FACTOR = 0.6

MAX_PROPS = 4
PROP_SPAWN_TIME = 3

OBSTACLE_SPAWN_TIME = 4


OBSERVATION_COUNT = 1000
FINAL_EPSILON = 0.0001 # final value of epsilon
EXPLORE = 300000  # frames over which to anneal epsilon
REPLAY_MEMORY = 80000 # number of previous transitions to remember
BATCH = 64 # minibatch size
GAMMA = 0.96 # decay rate of past observations original 0.99

OBJECTS_PATH = "Game/Assets/Objects/"
AUDIO_PATH = "Game/Assets/GameSounds/"
MIXER_VOLUME = 1.0

RECT_FOCUS_SURFACE = [0,50,800,600]

MAIN_MENU_SOUND = "MainMenuBackground.mp3"
RUN_SOUND = "Run.wav"
BUTTON_CLICK_SOUND = "ButtonClick.wav"
HIT_SOUND = "Hit.wav"
JUMP_SOUND = "Jump.wav"
SUCCESS_JUMP_SOUND = "SuccessfulJump.wav"