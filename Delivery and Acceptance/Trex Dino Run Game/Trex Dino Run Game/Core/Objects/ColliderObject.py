#-------------------------------------------------------------------------------------------------
# File: ColliderObject.py
#
# Author: Dakshvir Singh Rehill
# Date: 01/03/2021
#
# Summary: This class is an empty surface with a size to check for collisions
#-------------------------------------------------------------------------------------------------
import pygame
from Core.Systems.CollisionSystem import CollisionSystem
from Core.Systems.RenderSystem import RenderSystem
class ColliderObject():
    """This class is an empty surface with a size to check for collisions"""

    def __init__(self,size,position,tag):
        self.instance_id = -1
        self.rect = pygame.Rect(position,size)
        self.in_collision = False
        self.enabled = True
        self.tag = tag
        self.collided_with = []
        self.debug_mode = False
        #add to collision system
        CollisionSystem.get_instance().register_collider(self)
        RenderSystem.get_instance().register_debug_rects(self)

    def reset_object(self,size,position):
        self.rect = pygame.Rect(position,size)
        self.in_collision = False
        self.collided_with = []

    def swap_rect(self):
        temp = self.rect[2]
        self.rect[2] = self.rect[3]
        self.rect[3] = temp


    def cleanup(self):
        CollisionSystem.get_instance().remove_collider(self.instance_id)
        RenderSystem.get_instance().remove_rects(self.instance_id)