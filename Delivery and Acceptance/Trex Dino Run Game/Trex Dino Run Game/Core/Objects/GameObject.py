#-------------------------------------------------------------------------------------------------
# File: GameObject.py
#
# Author: Dakshvir Singh Rehill
# Date: 10/02/2021
#
# Summary: Each object in the game will be a gameobject to be able to move collide or render it
#-------------------------------------------------------------------------------------------------
from abc import ABC, abstractmethod

class GameObject(ABC):
    """Each object in the game will be a gameobject to be able to move collide or render it"""
    def __init__(self):
        """This method will instantiate a default game object"""
        self.instance_id = -1
        self.is_dirty = False
        self.surface = None
        self.rect = None
        self.spritesheet = None

    @abstractmethod
    def update(self,time_delta):
        pass

    @abstractmethod
    def cleanup(self):
        pass