#-------------------------------------------------------------------------------------------------
# File: InputSystem.py
#
# Author: Dakshvir Singh Rehill
# Date: 09/02/2021
#
# Summary: Input System class is responsible for getting input from pygame events and functions to 
# give input to the user during each update.
#-------------------------------------------------------------------------------------------------
import pygame,pygame_gui
class InputSystem:
    """Input System class is responsible for getting input from pygame events and functions to give input to the user during each update"""
    __instance = None
    @staticmethod
    def get_instance():
        """Function provides the default instance of input system"""
        if InputSystem.__instance == None:
            InputSystem()
        return InputSystem.__instance

    def initialize_system(self):
        self.input_dict = dict()

    def __init__(self):
        """ Virtually private constructor. """
        if InputSystem.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            InputSystem.__instance = self

    def set_ui_manager(self,manager):
        """Sets the UI Manager in the game"""
        self.ui_manager = manager

    def is_key_pressed(self,event_key):
        """Function returns true if the key was pressed this frame, else returns false"""
        if event_key not in self.input_dict: #first tracking request
            self.input_dict[event_key] = False
        return self.input_dict[event_key]


    def resolve_all_events(self):
        """Function resolves all events registered for input"""
        for event in pygame.event.get():
            # check if user quit the game
            if event.type == pygame.QUIT:
                return False
            # check for key events
            if event.type == pygame.KEYDOWN:
                for event_key in self.input_dict:
                    if event.key == event_key:
                        self.input_dict[event_key] = True
            if event.type == pygame.KEYUP:
                for event_key in self.input_dict:
                    if event.key == event_key:
                        self.input_dict[event_key] = False
            self.ui_manager.process_events(event)
        return True