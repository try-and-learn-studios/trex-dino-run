#-------------------------------------------------------------------------------------------------
# File: AudioSystem.py
#
# Author: Dakshvir Singh Rehill
# Date: 10/02/2021
#
# Summary: This system is responsible to play audio in the game
#-------------------------------------------------------------------------------------------------
from pygame import mixer
import glob
class AudioSystem():
    """This system is responsible to play audio in the game"""
    __instance = None
    @staticmethod
    def get_instance():
        """Function provides the default instance of audio system"""
        if AudioSystem.__instance == None:
            AudioSystem()
        return AudioSystem.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if AudioSystem.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            AudioSystem.__instance = self

    def initialize_system(self,audio_path,mixer_volume):
        mixer.init()
        self.sounds = dict()
        for audio_file in glob.glob(audio_path + "**/*.mp3",recursive = True):
            file_name = audio_file[audio_file.find("\\") + 1:]
            sound = mixer.Sound(audio_path + file_name)
            sound.set_volume(mixer_volume)
            self.sounds[file_name] = sound
        for audio_file in glob.glob(audio_path + "**/*.wav",recursive = True):
            file_name = audio_file[audio_file.find("\\") + 1:]
            sound = mixer.Sound(audio_path + file_name)
            sound.set_volume(mixer_volume)
            self.sounds[file_name] = sound

        self.toggle_mixer()

    def stop_all_sounds(self):
        if not self.enabled:
            return
        mixer.stop()

    def play_sound(self,sound_name, loops = 0):
        if not self.enabled:
            return
        if sound_name not in self.sounds:
            return
        self.sounds[sound_name].play(loops=loops)

    def stop_sound(self, sound_name):
        if not self.enabled:
            return
        if sound_name not in self.sounds:
            return
        self.sounds[sound_name].stop()

    def toggle_mixer(self,enable = True):
        self.enabled = enable