#-------------------------------------------------------------------------------------------------
# File: GameEngine.py
#
# Author: Dakshvir Singh Rehill
# Date: 09/02/2021
#
# Summary: This class controls the game loop and initializes all game
# components.
#-------------------------------------------------------------------------------------------------
import pygame
from Core.Systems.InputSystem import InputSystem
from Core.Systems.RenderSystem import RenderSystem
from Core.Systems.CollisionSystem import CollisionSystem
from Core.Systems.AudioSystem import AudioSystem
from Core.AI.TrainingSystem import training_loop
from multiprocess import Process,Lock, Value, Queue
from ctypes import c_bool

class GameEngine:
    """This class controls the game loop and initializes all game components."""
    def __init__(self,game_manager,window_size,focus_rect,default_color,caption, audio_path,mixer_volume):
        """This initializes pygame and all the respective systems of the game"""
        pygame.init()
        #initialize all other systems
        self.game_clock = pygame.time.Clock()
        InputSystem.get_instance().initialize_system()
        CollisionSystem.get_instance().initialize_system()
        RenderSystem.get_instance().initialize_system(window_size,focus_rect,default_color,caption)
        AudioSystem.get_instance().initialize_system(audio_path,mixer_volume)
        self.game_manager = game_manager
        if self.game_manager == None:
            raise Exception("No Game Manager")
        self.game_manager.initialize()
        InputSystem.get_instance().set_ui_manager(self.game_manager.ui_manager)
        RenderSystem.get_instance().set_ui_manager(self.game_manager.ui_manager)

    def game_loop(self):
        """This function loops through and provides updates to each system"""
        #initialize synchronized data
        pending_frames = Queue()
        pending_frame_lock = Lock()
        predictions = Queue()
        prediction_lock = Lock()
        is_training = Value(c_bool,lock=False)
        is_training.value = False
        is_simulated = Value(c_bool,lock=False)
        is_simulated.value = False
        train_mode = Value(c_bool,lock=False)
        train_mode.value = False
        quit = Value(c_bool,lock=False)
        quit.value = False
        system_ready = Value(c_bool,lock=False)
        system_ready.value = False
        train_loop = Process(target=training_loop,
                             args=(pending_frames,pending_frame_lock,predictions,prediction_lock,is_training,is_simulated,train_mode,quit,system_ready))
        train_loop.daemon = True
        train_loop.start()
        time_delta = self.game_clock.tick_busy_loop(25) / 1000.0 #this is the time it takes to complete one frame cycle
        while True:
            if not InputSystem.get_instance().resolve_all_events():
                quit.value = True
                break
            self.game_manager.update(time_delta,
                                     pending_frames,pending_frame_lock,predictions,prediction_lock,is_training,is_simulated,train_mode,quit,system_ready)
            CollisionSystem.get_instance().update()
            RenderSystem.get_instance().update_display()
            time_delta = self.game_clock.tick_busy_loop(25) / 1000.0 #this is the time it takes to complete one frame cycle
            