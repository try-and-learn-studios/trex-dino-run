#-------------------------------------------------------------------------------------------------
# File: CollisionSystem.py
#
# Author: Dakshvir Singh Rehill
# Date: 01/03/2021
#
# Summary: This class is a system that checks for all registered collisions in
# the game
#-------------------------------------------------------------------------------------------------
class CollisionSystem():
    """This class is a system that checks for all registered collisions in the game"""
    __instance = None
    @staticmethod
    def get_instance():
        """Function provides the default instance of collision system"""
        if CollisionSystem.__instance == None:
            CollisionSystem()
        return CollisionSystem.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if CollisionSystem.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            CollisionSystem.__instance = self

    def initialize_system(self):
        self.colliders = dict()

    def register_collider(self,collider_object):
        """Registers a surface with the system to check for collisions"""
        if collider_object.instance_id != -1: #don't register already registered object
            return
        collider_object.instance_id = len(self.colliders)
        self.colliders[collider_object.instance_id] = collider_object

    def remove_collider(self,instance_id):
        collider = self.colliders.pop(instance_id,None)
        if collider == None:
            return
        #remove the collider from other colliders if in collision
        tag = collider.tag + "_" + str(instance_id)
        for i in self.colliders:
            if self.colliders[i] == collider:
                continue
            if tag in self.colliders[i].collided_with:
                self.colliders[i].collided_with.remove(tag)

    def clear_system(self):
        self.colliders = dict()

    def update(self):
        looped_ix = []
        for i in self.colliders: #loop through the colliders
            looped_ix.append(i)
            for j in self.colliders:
                if j in looped_ix: #don't loop through already checked colliders
                    continue
                is_colliding = self.colliders[i].rect.colliderect(self.colliders[j].rect) #check if rects intersect
                self.colliders[i].in_collision = (self.colliders[i].in_collision or is_colliding)
                self.colliders[j].in_collision = (self.colliders[j].in_collision or is_colliding) #set in_collision
                j_tag = self.colliders[j].tag + "_" + str(j)
                i_tag = self.colliders[i].tag + "_" + str(i)
                if is_colliding and self.colliders[i].enabled and self.colliders[j].enabled: #check if enabled and colliding
                    if j_tag not in self.colliders[i].collided_with:
                        self.colliders[i].collided_with.append(j_tag)
                    if i_tag not in self.colliders[j].collided_with:
                        self.colliders[j].collided_with.append(i_tag)
                else: #if not enabled or not colliding
                    if j_tag in self.colliders[i].collided_with:
                        self.colliders[i].collided_with.remove(j_tag)
                    if i_tag in self.colliders[j].collided_with:
                        self.colliders[j].collided_with.remove(i_tag)
                self.colliders[i].in_collision = self.colliders[i].in_collision and len(self.colliders[i].collided_with) > 0
                self.colliders[j].in_collision = self.colliders[j].in_collision and len(self.colliders[j].collided_with) > 0





