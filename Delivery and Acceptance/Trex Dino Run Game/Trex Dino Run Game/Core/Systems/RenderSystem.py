#-------------------------------------------------------------------------------------------------
# File: RenderSystem.py
#
# Author: Dakshvir Singh Rehill
# Date: 10/02/2021
#
# Summary: Render System class is responsible for keeping track of the main pygame display and
# instantiate and track all renderable game surfaces
#-------------------------------------------------------------------------------------------------
import pygame
import cv2 #opencv
class RenderSystem():
    """Render System class is responsible for keeping track of the main pygame display and instantiate and track all renderable game surfaces"""
    __instance = None
    @staticmethod
    def get_instance():
        """Function provides the default instance of render system"""
        if RenderSystem.__instance == None:
            RenderSystem()
        return RenderSystem.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if RenderSystem.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            RenderSystem.__instance = self


    def initialize_system(self, window_size, focus_rect, window_default_color, caption):
        pygame.display.set_caption(caption)
        self.game_window = pygame.display.set_mode(window_size)
        self.focus_surface = pygame.Surface((focus_rect[2],focus_rect[3]))
        self.focus_rect = self.focus_surface.get_rect()
        self.focus_rect[0] = focus_rect[0]
        self.focus_rect[1] = focus_rect[1]
        self.window_size = window_size;
        self.default_color = window_default_color;
        self.debug_color = pygame.Color(250,10,10,200)
        self.drawables = dict()
        self.debug_rects = dict()
        self.toggle_renderer()

    def clear_system(self):
        self.drawables = dict()
        self.debug_rects = dict()

    def register_debug_rects(self,collider_object):
        self.debug_rects[collider_object.instance_id] = collider_object

    def remove_rects(self,instance_id):
        self.debug_rects.pop(instance_id,None)

    def register_drawable(self,game_object):
        """Registers a surface with the system to draw on screen"""
        if game_object.instance_id != -1: #don't register already registered object
            return
        game_object.instance_id = len(self.drawables)
        self.drawables[game_object.instance_id] = game_object

    def remove_drawable(self,instance_id):
        self.drawables.pop(instance_id,None)

    def set_ui_manager(self,manager):
        """Sets the UI Manager in the game"""
        self.ui_manager = manager

    def is_in_focus(self,rect):
        return self.focus_rect.colliderect(rect)

    def update_display(self):
        """Function clears the current screen and updates all components on the screen"""
        self.game_window.fill(self.default_color)
        self.focus_surface.fill(self.default_color)

        for instance_id in self.drawables:
            if self.drawables[instance_id].is_dirty:
                if self.is_in_focus(self.drawables[instance_id].rect):
                    self.focus_surface.blit(self.drawables[instance_id].surface, self.drawables[instance_id].rect)
                if self.enabled:
                    self.game_window.blit(self.drawables[instance_id].surface, self.drawables[instance_id].rect)
        if self.enabled:
            for instance_id in self.debug_rects:
                if self.debug_rects[instance_id].debug_mode:
                    pygame.draw.rect(self.game_window,self.debug_color,self.debug_rects[instance_id].rect,5)
        self.ui_manager.draw_ui(self.game_window)
        pygame.display.update()

    def get_display_array(self):
        img_arr = pygame.surfarray.array3d(self.focus_surface)
        return self.process_image(img_arr)

    def process_image(self,img_arr):
        img_arr = cv2.transpose(img_arr)
        img_arr = cv2.cvtColor(img_arr, cv2.COLOR_BGR2GRAY)
        img_arr = cv2.resize(img_arr,(80,80))
        return img_arr

    def toggle_renderer(self,enable = True):
        self.enabled = enable

def show_img(close,frame,lock):
    """Show images in new window"""
    while True:
        if frame.empty():
            continue
        if not lock.acquire(False):
            continue
        while not frame.empty():
            screen = frame.get_nowait()
        lock.release()
        window_title = "Game Play"
        cv2.namedWindow(window_title, cv2.WINDOW_NORMAL)        
        imS = cv2.resize(screen, (800, 400)) 
        cv2.imshow(window_title, screen)
        if (cv2.waitKey(1) & 0xFF == ord('q')) or close.value:
            cv2.destroyAllWindows()
            break






