#-------------------------------------------------------------------------------------------------
# File: TrainingSystem.py
#
# Author: Dakshvir Singh Rehill
# Date: 20/02/2021
#
# Summary: To allow for decoupled training behaviour, this class has functions
# to start training
#-------------------------------------------------------------------------------------------------
import numpy as np
import pandas as pd
import random
import os
import pickle
from collections import deque
from tensorflow.config.experimental import list_physical_devices,set_memory_growth
from tensorflow.keras.models import model_from_json
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.optimizers import SGD , Adam
import json
import Game.GameConfig as game_config
from collections import deque


class TrainingSystem():

    def __init__(self,debug=False):
        #allow for dynamic allocation of GPU memory to avoid OOM error
        gpu_devices = list_physical_devices("GPU")
        for device in gpu_devices:
            set_memory_growth(device, True)

        self.stacked_frames = None
        np.set_printoptions(threshold=np.inf)
        self.initial_state = None
        self.s_t = None
        self.new_state = False
        self.simulated = False
        self.final_epsilon = game_config.FINAL_EPSILON
        self.explore = game_config.EXPLORE
        self.replay_memory = game_config.REPLAY_MEMORY
        self.batch_size = game_config.BATCH
        self.gamma = game_config.GAMMA
        self.objects_path = game_config.OBJECTS_PATH
        self.observation_count = game_config.OBSERVATION_COUNT
        self.build_model()
        self.initialize_checkpoints()
        self.debug = debug
        if self.debug:
            self.initialize_data()

    def start_simulation(self,x_t):
        self.stacked_frames = deque([np.zeros((80,80), dtype=np.int) for i in range(4)], maxlen=4)
        for i in range(0,4):
            self.stacked_frames.append(x_t)
        self.initial_state = np.stack(self.stacked_frames,axis=-1)
        self.initial_state = np.expand_dims(self.initial_state,axis=0)
        self.s_t = self.initial_state
        self.new_state = True
        random_seeds = game_config.RANDOM_SEEDS
        random.shuffle(random_seeds)
        random.seed(random.choice(random_seeds))

    def normalize_frame(self,frame):
        frame = frame / 255.0
        return frame
        
    def stop_trainer(self):
        self.save_model()
        self.simulated = False
        self.s_t = None
        self.new_state = False
        self.initial_state = None

    def initialize_checkpoints(self):
        self.t = self.load_obj("time")
        if self.t is None:
            self.t = 0
            self.D = deque()
            self.epsilon = 0.1
            self.save_obj(self.t,"time")
            self.save_obj(self.D,"D")
            self.save_obj(self.epsilon,"epsilon")
        else:
            self.D = self.load_obj("D")
            self.epsilon = self.load_obj("epsilon")
        
    def initialize_data(self):
        self.loss_df = pd.read_csv(self.objects_path + "loss.csv") if os.path.isfile(self.objects_path + "loss.csv") else pd.DataFrame(columns =['loss'])
        self.scores_df = pd.read_csv(self.objects_path + "scores.csv") if os.path.isfile(self.objects_path + "scores.csv") else pd.DataFrame(columns = ['scores'])
        self.actions_df = pd.read_csv(self.objects_path + "actions.csv") if os.path.isfile(self.objects_path + "actions.csv") else pd.DataFrame(columns = ['actions'])
        self.q_values_df = pd.read_csv(self.objects_path + "qvalues.csv") if os.path.isfile(self.objects_path + "qvalues.csv") else pd.DataFrame(columns = ['qvalues'])

    def save_model(self):
        self.model.save_weights(self.objects_path + "model.h5", overwrite=True)
        self.save_obj(self.D,"D") #saving episodes
        self.save_obj(self.t,"time") #caching time steps
        self.save_obj(self.epsilon,"epsilon") #cache epsilon to avoid repeated randomness in actions
        if self.debug:
            self.loss_df.to_csv(self.objects_path + "loss.csv",index=False)
            self.scores_df.to_csv(self.objects_path + "scores.csv",index=False)
            self.actions_df.to_csv(self.objects_path + "actions.csv",index=False)
            self.q_values_df.to_csv(self.objects_path + "qvalues.csv",index=False)
        with open(self.objects_path + "model.json", "w") as outfile:
            json.dump(self.model.to_json(), outfile)

    def build_model(self):
        model = Sequential()
        model.add(Conv2D(32, (8, 8), padding='same',strides=(4, 4),input_shape=(80,80,4)))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Activation('relu'))
        model.add(Conv2D(64, (4, 4),strides=(2, 2),  padding='same'))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Activation('relu'))
        model.add(Conv2D(64, (3, 3),strides=(1, 1),  padding='same'))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Activation('relu'))
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dense(2))
        adam = Adam(lr=1e-4)
        model.compile(loss='mse',optimizer=adam)
        #create model file if not present
        if not os.path.isfile(self.objects_path + 'model.h5'):
            model.save_weights(self.objects_path + 'model.h5')
        else:
            model.load_weights(self.objects_path + 'model.h5')
        self.model = model
    
    def save_obj(self,obj, name):
        with open(self.objects_path + name + '.pkl', 'wb') as f: #dump files into objects folder
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
    
    def load_obj(self,name):
        if not os.path.isfile(self.objects_path + name + '.pkl'):
            return None
        with open(self.objects_path + name + '.pkl', 'rb') as f:
            return pickle.load(f)

    def get_pending_frame(self,pending_frames,pending_frame_lock):
        self.new_state = False
        if pending_frames.empty():
            return None
        if not pending_frame_lock.acquire(False):
            return None
        rew_img_tup = pending_frames.get_nowait()
        pending_frame_lock.release()
        self.new_state = True
        return rew_img_tup

def training_loop(pending_frames,pending_frame_lock,predictions,prediction_lock,is_training,is_simulated,train_mode,quit,system_ready):
    training_system = TrainingSystem() #set debug to true to store dataframes
    system_ready.value = True
    while True:
        if quit.value:
            return
        if not (training_system.simulated == is_simulated.value):
            training_system.simulated = is_simulated.value
            if training_system.simulated:
                training_system.train_mode = train_mode.value
            else:
                training_system.stop_trainer()
        if not training_system.simulated:
            while not pending_frames.empty():
                pending_frames.get_nowait()
            while not predictions.empty():
                predictions.get_nowait()
            continue
        if training_system.initial_state is None:
            rew_img_tup = training_system.get_pending_frame(pending_frames,pending_frame_lock)
            if rew_img_tup is not None:
                training_system.start_simulation(training_system.normalize_frame(rew_img_tup[1]))
            continue
        action_index = 0
        if training_system.new_state:
            if random.random() <= training_system.epsilon and training_system.train_mode:
                action_index = random.randint(0,1)
            else:
                action_index = np.argmax(training_system.model.predict(training_system.s_t))
            if prediction_lock.acquire(False):
                predictions.put_nowait(action_index)
                prediction_lock.release()

        rew_img_tup = training_system.get_pending_frame(pending_frames,pending_frame_lock)
        if rew_img_tup is None:
            continue

        if training_system.debug:
            training_system.actions_df.loc[len(training_system.actions_df)] = action_index
            training_system.scores_df.loc[len(training_system.scores_df)] = rew_img_tup[2]
        
        x_t1 = training_system.normalize_frame(rew_img_tup[1])
        training_system.stacked_frames.append(x_t1)
        s_t1 = np.stack(training_system.stacked_frames,axis=-1)
        s_t1 = np.expand_dims(s_t1,axis=0)
        if training_system.train_mode:
            loss = 0
            if training_system.epsilon > training_system.final_epsilon and training_system.t > training_system.observation_count:
                training_system.epsilon -= (0.1 - training_system.final_epsilon) / training_system.explore
            training_system.D.append((training_system.s_t, action_index, rew_img_tup[0], s_t1, rew_img_tup[0] < -0.5))
            if len(training_system.D) > training_system.replay_memory:
                training_system.D.popleft()
            if training_system.t > training_system.observation_count:
                is_training.value = True
                minibatch = random.sample(training_system.D, training_system.batch_size)
                state_tb = np.array([np.squeeze(sval[0]) for sval in minibatch],ndmin=3)
                action_tb = np.array([aval[1] for aval in minibatch])
                reward_tb = np.array([rval[2] for rval in minibatch])
                state_t1b = np.array([np.squeeze(s1val[3]) for s1val in minibatch],ndmin=3)
                terminal_b = np.array([tval[4] for tval in minibatch])
                Q_s1 = training_system.model.predict(state_t1b)
                target_tb = np.zeros((training_system.batch_size,2))
                for i in range(0, len(minibatch)):
                    Q_s1_max = np.max(Q_s1[i])
                    if terminal_b[i]:
                        target_tb[i,action_tb[i]] = reward_tb[i] # if terminated, only equals reward
                    else:
                        target = reward_tb[i] + training_system.gamma * Q_s1_max
                        target_tb[i,action_tb[i]] = target
                    if training_system.debug:
                        training_system.q_values_df.loc[len(training_system.q_values_df)] = Q_s1_max
                loss += training_system.model.train_on_batch(state_tb, target_tb)
                if training_system.debug:
                    training_system.loss_df.loc[len(training_system.loss_df)] = loss
                is_training.value = False
            training_system.t = training_system.t + 1   
            if rew_img_tup[0] < -0.5:
                is_training.value = True
                training_system.save_model()
                is_training.value = False
        training_system.s_t = training_system.initial_state if  rew_img_tup[0] < -0.5 else s_t1 #reset game to initial frame if terminate