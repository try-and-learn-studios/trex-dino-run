This repository consists of a Bot for Trex Dino Google Game.

The project has its own implementation of Trex Dino to allow for playability as well.

The delivery and acceptance folder has the Trex Dino Run Game project.

You can find the demo and presentation here:
[![Capstone Demo and Presentation](https://img.youtube.com/vi/EFZwBhGpnxw/0.jpg)](https://www.youtube.com/watch?v=EFZwBhGpnxw)

1. Run the Trex_Dino_Run_Game.py file or import and create and instance of the wrapper class provided in the file.

2. A pygame window will open up with a menu with 3 options.

4. Click Regular Mode button to see a sprite of the dino idle.

5. Press space or up key to start run animation on the dino.

6. Press space to jump.

7. Press fresh mode to train reinforcement learning bot.

8. Press view mode to see the trained bot in action.